Deisgn of the project is based on EBC(Entity-Boundary-Controller) design pattern.

Main components are:
-RestService-s which serves as BOUNDARY in ECB design pattern

-Service-s are the CONTROLLER component in the ECB. They orchestrate the workflow and may be reused by other boundaries than rest for example 

-Validator-s are used from Service-s 

-Repostirory - are used from Services

-Entities and DTO-s


Inside Application.java we have started a thread and scheduled a Runnable to read and save scheduled Products with a periodicity which may be configurable.


Techology stack:

Springboot

Hibernate

H2 in memory DB

MapStruct

Lombock

Maven


When running the app for the first time, execute maven build in order to let MapStruct to generate mappers

Swagger url:

http://localhost:8080/api/swagger-ui.html#/

To access H2:

URL:http://localhost:8080/api/h2-console

JDBC URL: jdbc:h2:mem:testdb

Driver class: org.h2.Driver

username and password: test