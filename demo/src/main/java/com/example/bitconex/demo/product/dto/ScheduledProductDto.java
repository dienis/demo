package com.example.bitconex.demo.product.dto;

import com.example.bitconex.demo.commons.dtoFields.DateTimeField;
import com.example.bitconex.demo.product.model.ScheduledProductStatus;

import lombok.Getter;
import lombok.Setter;
/**
 * @author dienis.mustafaj
 */
@Getter
@Setter
public class ScheduledProductDto extends AbstractProductDto{
	private DateTimeField scheduledTime;
	private Integer status;
	private String message;
}
