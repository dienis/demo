package com.example.bitconex.demo.product.dto;

import com.example.bitconex.demo.client.dto.ClientDto;
import com.example.bitconex.demo.commons.ValidatableDto;
import com.example.bitconex.demo.commons.dtoFields.BooleanField;
import com.example.bitconex.demo.commons.dtoFields.IntField;
import com.example.bitconex.demo.commons.dtoFields.StringField;

import lombok.Getter;
import lombok.Setter;
/**
 * @author dienis.mustafaj
 */
@Getter
@Setter
public class ProductDto extends AbstractProductDto{
	private BooleanField isActive;
}
