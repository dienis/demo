package com.example.bitconex.demo.client.dto;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import com.example.bitconex.demo.client.model.Client;

@Mapper(componentModel = "spring")
public interface ClientMapper {
	@Mappings({ @Mapping(source = "id", target = "id.value"),
		@Mapping(source = "name", target = "name.value"),
			@Mapping(source = "surname", target = "surname.value"),
			@Mapping(source = "email", target = "email.value") })

	ClientDto toDto(Client client);
	@InheritInverseConfiguration
	Client toEntity(ClientDto clientDto);
}
