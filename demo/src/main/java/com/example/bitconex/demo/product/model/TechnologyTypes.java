package com.example.bitconex.demo.product.model;

import java.util.Arrays;
/**
 * @author dienis.mustafaj
 */
public enum TechnologyTypes {
	VSDL(1), ISDN(2), XDSL(3);

	private TechnologyTypes(int code) {
		this.code = code;
	}

	private int code;

	public int getCode() {
		return code;
	}

	public static boolean codeExists(int code) {
		return Arrays.asList(TechnologyTypes.values()).stream()
				.anyMatch(technologyType -> (technologyType).getCode() == code);
	}
}
