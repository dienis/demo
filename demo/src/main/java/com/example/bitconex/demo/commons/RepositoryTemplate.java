package com.example.bitconex.demo.commons;

import java.lang.reflect.ParameterizedType;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Template for all repos.
 * Same as this template, we could create templates for 
 * Service layer or RestController layer, but for this demo, this is enough :)
 * @author  dienis.mustafaj
 */

@Getter
@AllArgsConstructor
public abstract class RepositoryTemplate<ENTITY extends BaseEntity> {
	@PersistenceContext
	protected EntityManager entityManager;


	private final Class<ENTITY> type;

	@SuppressWarnings("unchecked")
	public RepositoryTemplate() {
		type = (Class<ENTITY>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
	}

	public ENTITY save(ENTITY entity) {
		entityManager.persist(entity);
		return entity;
	}

	public ENTITY update(ENTITY entity) {
		entityManager.merge(entity);
		return entity;
	}

	public void delete(ENTITY entity) {
		entity.setDeleted(true);
		update(entity);
	}

	public Optional<ENTITY> findById(Long id) {
		return java.util.Optional.ofNullable(entityManager.find(type, id));
	}

}
