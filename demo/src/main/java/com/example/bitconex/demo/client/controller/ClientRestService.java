package com.example.bitconex.demo.client.controller;

import java.util.List;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.bitconex.demo.client.dto.ClientDto;
import com.example.bitconex.demo.client.service.ClientService;
import com.example.bitconex.demo.commons.rest.RequestHandler;
import com.example.bitconex.demo.commons.rest.RestResponse;
import com.example.bitconex.demo.product.dto.ProductDto;
import com.example.bitconex.demo.product.dto.ScheduledProductDto;

import lombok.AllArgsConstructor;

@RestController
@RequestMapping(path = "/clients")
@CrossOrigin(origins = "*")
@AllArgsConstructor
public class ClientRestService {
private ClientService clientService;
	
	@PostMapping(path = { "" })
	public RestResponse<ClientDto> save(@RequestBody ClientDto dto) {
		return new RequestHandler().handle(() -> clientService.save(dto));
	}
	
	@GetMapping("/{id}/products")
	public RestResponse<List<ProductDto>> findProducts(@PathVariable Long id) {
		return new RequestHandler().handle(() -> clientService.findAllProducts(id));
	}
	
	@GetMapping("/{id}/scheduledProducts")
	public RestResponse<List<ScheduledProductDto>> findScheduledProducts(@PathVariable Long id) {
		return new RequestHandler().handle(() -> clientService.findAllScheduledProducts(id));
	}
}
