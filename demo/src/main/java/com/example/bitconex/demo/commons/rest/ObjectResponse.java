package com.example.bitconex.demo.commons.rest;

import lombok.Getter;
import lombok.Setter;
/**
 * This is the object that is used to communicate through rest api
 * @author dienis.mustafaj
 */
@Getter
@Setter
public class ObjectResponse<ENTITY> {
	private ENTITY payload;
	private Integer status;
	private String message;
}
