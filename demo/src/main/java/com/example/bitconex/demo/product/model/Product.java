package com.example.bitconex.demo.product.model;

import javax.persistence.Entity;

import lombok.Getter;
import lombok.Setter;
/**
 * @author dienis.mustafaj
 */
@Getter
@Setter
@Entity
public class Product extends AbstractProduct{
	private Boolean isActive = true;
}
