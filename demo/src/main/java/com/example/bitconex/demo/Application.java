package com.example.bitconex.demo;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.ApplicationContext;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.example.bitconex.demo.product.service.ScheduledProductService;

@SpringBootApplication
@EnableTransactionManagement
@EnableConfigurationProperties
public class Application {
	
	private static int HOURS = 12;

	public static void main(String[] args) {
		ApplicationContext app  = SpringApplication.run(Application.class, args);
		
		/**
		 * Here, we SIMULATE a batch application, which is scheduled to read data from table where we store scheduled products, and store them
		 * in the table where we have products in "live" env.
		 * */
		ScheduledProductService scheduledProductService =  app.getBean(ScheduledProductService.class);
		ScheduledExecutorService scheduledExecutorService= Executors.newSingleThreadScheduledExecutor();
		scheduledExecutorService.scheduleAtFixedRate(scheduledProductService::readScheduledProductsAndSave, 0, HOURS, TimeUnit.HOURS);
		
	}

}
