package com.example.bitconex.demo.product.model;

import java.time.LocalDateTime;

import javax.persistence.Entity;

import lombok.Getter;
import lombok.Setter;

/**
 * @author dienis.mustafaj
 */
@Getter
@Setter
@Entity
public class ScheduledProduct extends AbstractProduct {
	private LocalDateTime scheduledTime;
	private Integer status = ScheduledProductStatus.SCHEDULED.getCode();
	private String message = ScheduledProductStatus.SCHEDULED.getMessage();
}
