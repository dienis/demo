package com.example.bitconex.demo.product.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.bitconex.demo.commons.rest.RequestHandler;
import com.example.bitconex.demo.commons.rest.RestResponse;
import com.example.bitconex.demo.product.dto.ProductDto;
import com.example.bitconex.demo.product.dto.ScheduledProductDto;
import com.example.bitconex.demo.product.service.ScheduledProductService;

import lombok.AllArgsConstructor;
/**
 * @author dienis.mustafaj
 */

@RestController
@RequestMapping(path = "/productsScheduler")
@CrossOrigin(origins = "*")
@AllArgsConstructor
public class ProductSchedulerRestService {
	private ScheduledProductService scheduledProductService;
	
	@PostMapping(path = { "" })
	public RestResponse<ScheduledProductDto> schedule(@RequestBody ScheduledProductDto dto) {
		return new RequestHandler().handle(() -> scheduledProductService.schedule(dto));
	}
}
