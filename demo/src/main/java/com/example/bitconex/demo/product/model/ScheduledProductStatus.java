package com.example.bitconex.demo.product.model;
/**
 * @author dienis.mustafaj
 */
public enum ScheduledProductStatus {
	SUCCESS(1, "SUCCESS"), FAILED(2, "FAILED"), SCHEDULED(3, "SCHEDULED");

	private ScheduledProductStatus(int code, String message) {
		this.code = code;
		this.message = message;
	}

	private int code;
	private String message;

	public int getCode() {
		return code;
	}
	
	public String getMessage() {
		return message;
	}
}
