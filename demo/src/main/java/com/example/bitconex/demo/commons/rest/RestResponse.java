package com.example.bitconex.demo.commons.rest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import lombok.Getter;
import lombok.Setter;

/**
 * To remove ResponseEntity boilerplate :)
 * 
 * @author dienis.mustafaj
 */
@Getter
@Setter
public class RestResponse<ENTITY> extends ResponseEntity<ObjectResponse<ENTITY>> {

	public RestResponse(ObjectResponse<ENTITY> body, HttpStatus status) {
		super(body, status);
	}

}
