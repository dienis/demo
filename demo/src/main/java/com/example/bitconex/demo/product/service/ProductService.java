package com.example.bitconex.demo.product.service;

import java.util.List;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.bitconex.demo.product.dto.ProductDto;
import com.example.bitconex.demo.product.dto.ProductMapper;
import com.example.bitconex.demo.product.model.Product;
import com.example.bitconex.demo.product.repository.ProductRepository;
import com.example.bitconex.demo.product.validation.ProductValidator;

/**
 * @author dienis.mustafaj
 */
@Service
@Transactional
public class ProductService {
	private @Autowired ProductRepository productRepository;
	private @Autowired ProductValidator productValidator;
	private @Autowired ProductMapper mapper;

	public ProductDto update(ProductDto productDto) {
		return productValidator.validate(productDto)
				? mapper.toDto(productRepository.update(mapper.toEntity(productDto)))
				: productDto;
	}

	public ProductDto save(ProductDto productDto) {
		return productValidator.validate(productDto) ? mapper.toDto(productRepository.save(mapper.toEntity(productDto)))
				: productDto;
	}

	public Product saveWithoutValidation(Product product) {
		return productRepository.save(product);
	}

	public Void delete(ProductDto productDto) {
		productRepository.delete(mapper.toEntity(productDto));
		return null;
	}

	public List<ProductDto> findByClientId(Long clientId) {
		return productRepository.findByClientId(clientId).stream().map(mapper::toDto).collect(Collectors.toList());
	}

	public void deactivateClientActiveProduct(Long clientId) {
		productRepository.deactivateClientActiveProduct(clientId);
	}

	public boolean clientHasActiveProductOtherThan(Long clientId, Long productId) {
		return productRepository.clientHasActiveProductOtherThan(clientId, productId);
	}

	public boolean productWithNameAndClientIdExits(String name, Long clientId) {
		return productRepository.productWithNameAndClientIdExits(name, clientId);
	}

	public Object activateByProductIdAndClientId(ProductDto productDto) {
		productDto.getIsActive().setValid(true);
		boolean isValid = productValidator.validateForActivation(productDto);
		if (isValid) {
			productRepository.activateByProductIdAndClientId(productDto.getId(),
					productDto.getClient().getId().getValue());
			return null;
		}
		return productDto;

	}

	public Void deactivateByProductIDAndClientId(ProductDto productDto) {
		productRepository.deactivateById(productDto.getId(), productDto.getClient().getId().getValue());
		return null;
	}
}
