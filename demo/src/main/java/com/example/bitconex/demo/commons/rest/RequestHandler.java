package com.example.bitconex.demo.commons.rest;

import java.util.concurrent.Callable;

import org.springframework.http.HttpStatus;

import com.example.bitconex.demo.commons.RequestCantBeProcessed;
import com.example.bitconex.demo.commons.ValidatableDto;

import lombok.NonNull;
/**
 * This is "the handler" of all requests from rest api, it will format prepare and format the response
 * @author dienis.mustafaj
 */
public class RequestHandler {
	
	public static final int OK_HTTP_STATUS = 200;
	public static final int BAD_REQUEST = 400;
	public static final int SERVER_ERROR = 500;

	public <T> RestResponse<T> handle(@NonNull Callable<T> operation) {
		ObjectResponse<T> response = new ObjectResponse<>();

		try {
			T payload = operation.call();
			buildResponseOnNoException(response, payload);
		}catch(RequestCantBeProcessed e) {
			response.setPayload(null);
			response.setMessage(e.getMessage());
			response.setStatus(BAD_REQUEST);
		} 
		 catch (Exception e) {
			//add logger
			response.setPayload(null);
			response.setMessage("Server Errror");
			response.setStatus(SERVER_ERROR);
			e.printStackTrace();
		}
		return new RestResponse<T>(response, HttpStatus.OK);
	}
	
	private <T> void buildResponseOnNoException(ObjectResponse<T> response, T payload) {
		
		response.setPayload(payload);
		if(payload instanceof ValidatableDto) {
			if(((ValidatableDto)payload).isValid) {
				response.setStatus(OK_HTTP_STATUS);
				response.setMessage("OK");
			}else {
				response.setStatus(BAD_REQUEST);
				response.setMessage("Request payload is not valid");
			}
		}else {
			response.setStatus(OK_HTTP_STATUS);
			response.setMessage("OK");
		}
	}

}
