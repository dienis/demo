package com.example.bitconex.demo.product.repository;

import java.util.List;

import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import com.example.bitconex.demo.commons.RepositoryTemplate;
import com.example.bitconex.demo.commons.RequestCantBeProcessed;
import com.example.bitconex.demo.product.model.Product;
/**
 * @author dienis.mustafaj
 */
@Repository
public class ProductRepository extends RepositoryTemplate<Product> {

	public void activateByProductIdAndClientId(Long productId, Long clientId) {
		changeActiveStatusByProductIdAndClientId(productId, clientId, true);
	}

	public void deactivateById(Long productId, Long clientId) {
		changeActiveStatusByProductIdAndClientId(productId, clientId, false);
	}

	public List<Product> findByClientId(Long clientId) {
		Query query = entityManager
				.createQuery("select p from Product p where p.client.id = :clientId and isDeleted = :isDeleted");

		query.setParameter("clientId", clientId);
		query.setParameter("isDeleted", false);

		return query.getResultList();
	}
	
	public boolean productWithNameAndClientIdExits(String name, Long clientId) {
		Query query = entityManager.createQuery(
				"select p from Product p where p.client.id = :clientId  and name = :name and isDeleted = :isDeleted");

		query.setParameter("clientId", clientId);
		query.setParameter("name", name);
		query.setParameter("isDeleted", false);

		int size = query.getResultList().size();
		return size > 0;
	}

	public boolean clientHasActiveProductOtherThan(Long clientId, Long productId) {
		Query query = entityManager.createQuery(
				"select p from Product p where p.client.id = :clientId and id <> :productId and isActive = :isActive and isDeleted = :isDeleted");

		query.setParameter("clientId", clientId);
		query.setParameter("productId", productId != null ? productId : -1);
		query.setParameter("isActive", true);
		query.setParameter("isDeleted", false);

		int size = query.getResultList().size();
		return size > 0;
	}

	private void changeActiveStatusByProductIdAndClientId(Long productId, Long clientId, boolean active) {
		Query query = entityManager.createQuery(
				"update Product p set p.isActive = :active where p.client.id = :clientId and p.id = :productId");// jpql
		query.setParameter("active", active);
		query.setParameter("clientId", clientId);
		query.setParameter("productId", productId);
		int recordsUpdated = query.executeUpdate();
		if (recordsUpdated == 0) {
			throw new RequestCantBeProcessed("Product with id: " + productId + " for the client does not exists!");
		}
	}

	public void deactivateClientActiveProduct(Long clientId) {
		Query query = entityManager
				.createQuery("update Product p set p.isActive = :active where p.client.id = :clientId ");
		query.setParameter("active", false);
		query.setParameter("clientId", clientId);
		query.executeUpdate();
	}
}
