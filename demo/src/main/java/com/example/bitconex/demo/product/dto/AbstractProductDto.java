package com.example.bitconex.demo.product.dto;

import com.example.bitconex.demo.client.dto.ClientDto;
import com.example.bitconex.demo.commons.ValidatableDto;
import com.example.bitconex.demo.commons.dtoFields.IntField;
import com.example.bitconex.demo.commons.dtoFields.StringField;

import lombok.Getter;
import lombok.Setter;

/**
 * @author dienis.mustafaj
 */
@Getter
@Setter
public abstract class AbstractProductDto extends ValidatableDto{
	private Long id;
	private StringField name;
	private StringField upstream;
	private IntField technologyType;
	private IntField deviceType;
	private ClientDto client;
}
