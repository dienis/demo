package com.example.bitconex.demo.product.service;

import java.util.List;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.bitconex.demo.product.dto.ScheduledProductDto;
import com.example.bitconex.demo.product.dto.ScheduledProductMapper;
import com.example.bitconex.demo.product.model.Product;
import com.example.bitconex.demo.product.model.ScheduledProduct;
import com.example.bitconex.demo.product.model.ScheduledProductStatus;
import com.example.bitconex.demo.product.repository.ScheduledProductRepository;
import com.example.bitconex.demo.product.validation.ScheduledProductValidator;

/**
 * @author dienis.mustafaj
 */
@Service
@Transactional
public class ScheduledProductService {
	private @Autowired ProductService productService;
	private @Autowired ScheduledProductRepository scheduledProductRepository;
	private @Autowired ScheduledProductValidator scheduledProductValidator;
	private @Autowired ScheduledProductMapper mapper;

	public List<ScheduledProductDto> findByClientId(Long clientId) {
		return scheduledProductRepository.findByClientId(clientId).stream().map(mapper::toDto)
				.collect(Collectors.toList());
	}

	public ScheduledProductDto schedule(ScheduledProductDto dto) {
		dto.setStatus(ScheduledProductStatus.SCHEDULED.getCode());
		dto.setMessage(ScheduledProductStatus.SCHEDULED.getMessage());
		return scheduledProductValidator.validate(dto)
				? mapper.toDto(scheduledProductRepository.save(mapper.toEntity(dto)))
				: dto;
	}

	public void readScheduledProductsAndSave() {
		List<ScheduledProduct> productsReadyToSave = scheduledProductRepository.findScheduledProductsReadyToSave();
		productsReadyToSave.forEach(this::saveSingleScheduledProduct);
	}

	private void saveSingleScheduledProduct(ScheduledProduct scheduledProduct) {
		try {
			productService.deactivateClientActiveProduct(scheduledProduct.getClient().getId());
			productService.saveWithoutValidation(convertoToProduct(scheduledProduct));
			scheduledProductRepository.updateStatusAndMessage(ScheduledProductStatus.SUCCESS.getCode(),
					ScheduledProductStatus.SUCCESS.getMessage(), scheduledProduct.getId());
		} catch (Exception e) {
			scheduledProductRepository.updateStatusAndMessage(ScheduledProductStatus.FAILED.getCode(), e.getMessage(),
					scheduledProduct.getId());
		}
	}

	public boolean productWithNameAndClientIdExits(String name, Long clientId) {
		return scheduledProductRepository.productWithNameAndClientIdExits(name, clientId);
	}

	private Product convertoToProduct(ScheduledProduct scheduledProduct) {
		Product product = new Product();

		product.setClient(scheduledProduct.getClient());
		product.setDeviceType(scheduledProduct.getDeviceType());
		product.setName(scheduledProduct.getName());
		product.setTechnologyType(scheduledProduct.getTechnologyType());
		product.setUpstream(scheduledProduct.getUpstream());

		return product;
	}

}
