package com.example.bitconex.demo.product.validation;

import java.util.function.Predicate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.example.bitconex.demo.client.dto.ClientDto;
import com.example.bitconex.demo.client.service.ClientService;
import com.example.bitconex.demo.commons.Invalidator;
import com.example.bitconex.demo.commons.Validator;
import com.example.bitconex.demo.commons.dtoFields.GenericField;
import com.example.bitconex.demo.commons.dtoFields.IntField;
import com.example.bitconex.demo.commons.dtoFields.LongField;
import com.example.bitconex.demo.product.dto.ProductDto;
import com.example.bitconex.demo.product.model.DeviceTypes;
import com.example.bitconex.demo.product.model.TechnologyTypes;
import com.example.bitconex.demo.product.service.ProductService;
import com.example.bitconex.demo.product.service.ScheduledProductService;

/**
 * @author dienis.mustafaj
 */
@Component
public class ProductValidator extends Validator<ProductDto> {

	private static final String USER_MUST_HAVE_ONLY_ONE_PRODUCT_ACTIVE = "User must have only one product active";
	private static final String DEVICE_TYPE_DOES_NOT_EXISTS = "Device type does not exists";
	private static final String TECHNOLOGY_TYPE_DOES_NOT_EXISTS = "Technology type does not exists";
	private static final String CLIENT_DOES_NOT_EXISTS = "Client with this id does not exists!";
	public static final String PRODUCT_WITH_THIS_NAME_ALREADY_EXISTS_FOR_THE_CLIENT = "Product with this name already exists for the client!";

	private @Autowired ProductService productService;
	private @Autowired ScheduledProductService scheduledProductService;
	private @Autowired ClientService clientService;

	@Override
	protected boolean doValdiate(ProductDto dto) {
		boolean isValid = true;

		isValid &= validateClient(dto.getClient(), e -> clientService.findById(e.getValue()).isPresent());
		isValid &= validateActiveStatus(dto);
		isValid &= validateDeviceType(dto.getDeviceType());
		isValid &= validateTechnologyType(dto.getTechnologyType());
		isValid &= validateName(dto);

		return isValid;

	}

	private boolean validateActiveStatus(ProductDto dto) {
		if (dto.getIsActive().getValue()
				&& productService.clientHasActiveProductOtherThan(dto.getClient().getId().getValue(), dto.getId())) {
			return validateForActivation(dto);
		}
		return true;
	}

	public static boolean validateClient(ClientDto client, Predicate<LongField> clientExistsPredicate) {
		if (!clientExistsPredicate.test(client.getId())) {
			return Invalidator.invalidateField(client.getId(), CLIENT_DOES_NOT_EXISTS);
		}
		return true;
	}

	public static boolean validateDeviceType(IntField field) {
		if (!isFieldPopulated(field)) {
			return Invalidator.invalidateRequiredField(field);
		} else if (!DeviceTypes.codeExists(field.getValue())) {
			return Invalidator.invalidateField(field, DEVICE_TYPE_DOES_NOT_EXISTS);
		}
		return true;
	}

	public static boolean validateTechnologyType(IntField field) {
		if (!isFieldPopulated(field)) {
			return Invalidator.invalidateRequiredField(field);
		} else if (!TechnologyTypes.codeExists(field.getValue())) {
			return Invalidator.invalidateField(field, TECHNOLOGY_TYPE_DOES_NOT_EXISTS);
		}
		return true;
	}

	private boolean validateName(ProductDto dto) {
		if (!isFieldPopulated(dto.getName()) || dto.getName().getValue().isEmpty()) {
			return Invalidator.invalidateRequiredField(dto.getName());
		}
		if (productService.productWithNameAndClientIdExits(dto.getName().getValue(),
				dto.getClient().getId().getValue())) {
			return Invalidator.invalidateField(dto.getName(), PRODUCT_WITH_THIS_NAME_ALREADY_EXISTS_FOR_THE_CLIENT);
		}
		if (scheduledProductService.productWithNameAndClientIdExits(dto.getName().getValue(),
				dto.getClient().getId().getValue())) {
			return Invalidator.invalidateField(dto.getName(),
					ScheduledProductValidator.SCHEDULED_PRODUCT_WITH_THIS_NAME_ALREADY_EXISTS_FOR_THE_CLIENT);
		}
		return true;
	}

	public boolean validateForActivation(ProductDto dto) {
		boolean isValid = true;

		if (!clientService.findById(dto.getClient().getId().getValue()).isPresent()) {
			isValid &= Invalidator.invalidateField(dto.getClient().getId(), CLIENT_DOES_NOT_EXISTS);
		}
		if (productService.clientHasActiveProductOtherThan(dto.getClient().getId().getValue(), dto.getId())) {
			isValid &= Invalidator.invalidateField(dto.getClient().getId(), USER_MUST_HAVE_ONLY_ONE_PRODUCT_ACTIVE);
			dto.isValid = false;
		}
		
		return isValid;
	}

	private static boolean isFieldPopulated(GenericField<?> field) {
		return field.getValue() != null;
	}

}
