package com.example.bitconex.demo.client.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.OneToMany;

import com.example.bitconex.demo.commons.BaseEntity;
import com.example.bitconex.demo.product.model.Product;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
public class Client extends BaseEntity{
	private String name;
	private String surname;
	private String email;
	
	@OneToMany(mappedBy="client")
    private Set<Product> items = new HashSet<Product>();
}
