package com.example.bitconex.demo.product.dto;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import com.example.bitconex.demo.client.dto.ClientMapper;
import com.example.bitconex.demo.product.model.Product;
/**
 * @author dienis.mustafaj
 */
@Mapper(componentModel = "spring", uses = { ClientMapper.class })
public interface ProductMapper {
	@Mappings({ @Mapping( target= "id", source  = "id"),
		@Mapping( target= "isActive.value",source  = "isActive"),
			@Mapping( target = "name.value", source = "name"),
			@Mapping( target= "upstream.value", source  = "upstream"),
			@Mapping( target= "technologyType.value", source  = "technologyType"),
			@Mapping( target = "deviceType.value", source  = "deviceType"), })

	ProductDto toDto(Product product);
	@InheritInverseConfiguration
	Product toEntity(ProductDto productDto);

}
