package com.example.bitconex.demo.commons;

import com.example.bitconex.demo.commons.dtoFields.GenericField;
/**
 * Helper class for invalidators
 * @author dienis.mustafaj
 */
public class Invalidator {

	public static final String REQUIRED_FIELD_MESSAGE = "Field is not populated!";

	public static boolean invalidateField(GenericField<?> field, String errorMessage) {
		field.getErrorMessages().add(errorMessage);
		field.setValid(false);
		return false;
	}

	public static boolean invalidateRequiredField(GenericField<?> field) {
		return invalidateField(field, REQUIRED_FIELD_MESSAGE);
	}
}
