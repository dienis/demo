package com.example.bitconex.demo.client.dto;

import com.example.bitconex.demo.commons.dtoFields.LongField;
import com.example.bitconex.demo.commons.dtoFields.StringField;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ClientDto {
	private LongField id;
	private StringField name;
	private StringField surname;
	private StringField email;
}
