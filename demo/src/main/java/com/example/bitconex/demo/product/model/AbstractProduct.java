package com.example.bitconex.demo.product.model;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

import com.example.bitconex.demo.client.model.Client;
import com.example.bitconex.demo.commons.BaseEntity;

import lombok.Getter;
import lombok.Setter;
/**
 * @author dienis.mustafaj
 */
@Getter
@Setter
@MappedSuperclass
public abstract class AbstractProduct extends BaseEntity {
	private String name;
	private String upstream;
	private Integer technologyType;
	private Integer deviceType;

	@ManyToOne
	@JoinColumn
	private Client client;
}
