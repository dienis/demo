package com.example.bitconex.demo.commons;

import java.util.ArrayList;
import java.util.List;

import com.example.bitconex.demo.commons.rest.RequestHandler;
/**
 * Base class for all validatable DTO-s
 * @see Validator
 * @author dienis.mustafaj
 */
public abstract class ValidatableDto {
	public boolean isValid = true;
	public List<String> errorMessages = new ArrayList<>();
}
