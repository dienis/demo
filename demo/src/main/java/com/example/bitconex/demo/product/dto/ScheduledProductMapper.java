package com.example.bitconex.demo.product.dto;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import com.example.bitconex.demo.client.dto.ClientMapper;
import com.example.bitconex.demo.product.model.ScheduledProduct;
/**
 * @author dienis.mustafaj
 */
@Mapper(componentModel = "spring", uses = { ClientMapper.class })
public interface ScheduledProductMapper {
	@Mappings({ @Mapping( target= "id", source  = "id"),
		@Mapping( target= "scheduledTime.value",source  = "scheduledTime"),
			@Mapping( target = "name.value", source = "name"),
			@Mapping( target= "upstream.value", source  = "upstream"),
			@Mapping( target= "technologyType.value", source  = "technologyType"),
			@Mapping( target = "deviceType.value", source  = "deviceType"), })

	ScheduledProductDto toDto(ScheduledProduct product);
	@InheritInverseConfiguration
	ScheduledProduct toEntity(ScheduledProductDto productDto);
}
