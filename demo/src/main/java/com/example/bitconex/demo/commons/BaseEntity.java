package com.example.bitconex.demo.commons;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * This is the base class for all classes that are mapped using JPA
 * @author  dienis.mustafaj
 */

@Getter
@Setter
@EqualsAndHashCode
@ToString
@MappedSuperclass
public abstract class BaseEntity {
	@Id
	@GeneratedValue
	private Long id;
	private boolean isDeleted;	
}
