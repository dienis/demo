package com.example.bitconex.demo.commons.dtoFields;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;
/**
 * This is base class for all validatable fields used in DTO-s
 * @see BooleanField etc
 * @author  dienis.mustafaj
 */
@Getter
@Setter
public class GenericField<TYPE> {
	private TYPE value;
	private boolean isValid = true;
	private List<String> errorMessages = new ArrayList<>();
}
