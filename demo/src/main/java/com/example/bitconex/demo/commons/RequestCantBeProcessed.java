package com.example.bitconex.demo.commons;

import com.example.bitconex.demo.commons.rest.RequestHandler;

/**
 * Our Runtime exception to understand when the request payload is not correct
 * @see RequestHandler
 * @author dienis.mustafaj
 */
public class RequestCantBeProcessed extends RuntimeException {
	public RequestCantBeProcessed(String message) {
		super(message);
	}
}
