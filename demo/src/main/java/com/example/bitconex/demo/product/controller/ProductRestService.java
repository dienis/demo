package com.example.bitconex.demo.product.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.bitconex.demo.commons.rest.RequestHandler;
import com.example.bitconex.demo.commons.rest.RestResponse;
import com.example.bitconex.demo.product.dto.ProductDto;
import com.example.bitconex.demo.product.service.ProductService;

import lombok.AllArgsConstructor;
/**
 * @author dienis.mustafaj
 */

@RestController
@RequestMapping(path = "/products")
@CrossOrigin(origins = "*")
@AllArgsConstructor
public class ProductRestService {
	private ProductService productService;
	
	@PostMapping(path = { "" })
	public RestResponse<ProductDto> save(@RequestBody ProductDto dto) {
		return new RequestHandler().handle(() -> productService.save(dto));
	}
	
	@PostMapping(path = { "activate"})
	public RestResponse<Object> activate(@RequestBody ProductDto dto) {
		return new RequestHandler().handle(() -> productService.activateByProductIdAndClientId(dto));
	}
	
	@PostMapping(path = { "deactivate" })
	public RestResponse<Void> deActivate(@RequestBody ProductDto dto) {
		return new RequestHandler().handle(() -> productService.deactivateByProductIDAndClientId(dto));
	}
	
	@DeleteMapping(path = { "" })
	public RestResponse<Void> delete(@RequestBody ProductDto dto) {
		return new RequestHandler().handle(() -> productService.delete(dto));
	}
}
