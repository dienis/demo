package com.example.bitconex.demo.commons;

import java.util.List;

/**
 * Base class for all DTO validators
 * 
 * @author dienis.mustafaj
 */
public abstract class Validator<DTO extends ValidatableDto> {

	protected abstract boolean doValdiate(DTO dto);
	
	public boolean validate(DTO dto) {
		return dto.isValid = doValdiate(dto);
	}

	public void validateAll(List<DTO> dtoList) {
		dtoList.forEach(this::validate);
	}

}
