package com.example.bitconex.demo.client.repository;

import org.springframework.stereotype.Repository;

import com.example.bitconex.demo.client.model.Client;
import com.example.bitconex.demo.commons.RepositoryTemplate;

@Repository
public class ClientRepository extends RepositoryTemplate<Client>{

}
