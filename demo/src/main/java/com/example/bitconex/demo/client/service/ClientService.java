package com.example.bitconex.demo.client.service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.bitconex.demo.client.dto.ClientDto;
import com.example.bitconex.demo.client.dto.ClientMapper;
import com.example.bitconex.demo.client.repository.ClientRepository;
import com.example.bitconex.demo.product.dto.ProductDto;
import com.example.bitconex.demo.product.dto.ScheduledProductDto;
import com.example.bitconex.demo.product.service.ProductService;
import com.example.bitconex.demo.product.service.ScheduledProductService;

@Service
@Transactional
public class ClientService {
	private @Autowired ClientRepository clientRepository;
	private @Autowired ClientMapper clientMapper;
	private @Autowired ProductService productService;
	private @Autowired ScheduledProductService scheduledProductService;

	public ClientDto save(ClientDto client) {
		// no validation for client :-)
		return clientMapper.toDto(clientRepository.save(clientMapper.toEntity(client)));
	}

	public Optional<ClientDto> findById(Long id) {
		return clientRepository.findById(id).flatMap(e -> Optional.of(clientMapper.toDto(e)));
	}

	public List<ProductDto> findAllProducts(Long clientId) {
		return productService.findByClientId(clientId);
	}
	
	public List<ScheduledProductDto> findAllScheduledProducts(Long clientId) {
		return scheduledProductService.findByClientId(clientId);
	}
}
