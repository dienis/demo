package com.example.bitconex.demo.product.validation;

import static com.example.bitconex.demo.product.validation.ProductValidator.validateClient;
import static com.example.bitconex.demo.product.validation.ProductValidator.validateDeviceType;
import static com.example.bitconex.demo.product.validation.ProductValidator.validateTechnologyType;

import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.example.bitconex.demo.client.service.ClientService;
import com.example.bitconex.demo.commons.Invalidator;
import com.example.bitconex.demo.commons.Validator;
import com.example.bitconex.demo.commons.dtoFields.DateTimeField;
import com.example.bitconex.demo.commons.dtoFields.GenericField;
import com.example.bitconex.demo.product.dto.ScheduledProductDto;
import com.example.bitconex.demo.product.service.ProductService;
import com.example.bitconex.demo.product.service.ScheduledProductService;
/**
 * @author dienis.mustafaj
 */
@Component
public class ScheduledProductValidator extends Validator<ScheduledProductDto> {

	public static final String SCHEDULED_PRODUCT_WITH_THIS_NAME_ALREADY_EXISTS_FOR_THE_CLIENT = "Scheduled product  with this name already exists for the client!";
	private static final String SCHEDULED_DATE_MUST_BE_AFTER_TODAY = "Scheduled date must be after today";
	
	private @Autowired ScheduledProductService scheduledProductService;
	private @Autowired ProductService productService;
	private @Autowired ClientService clientService;

	@Override
	protected boolean doValdiate(ScheduledProductDto dto) {
		boolean isValid = true;

		isValid &= validateClient(dto.getClient(), e -> clientService.findById(e.getValue()).isPresent());
		isValid &= validateDeviceType(dto.getDeviceType());
		isValid &= validateTechnologyType(dto.getTechnologyType());
		isValid &= validateName(dto);
		isValid &= validateScheduledTime(dto.getScheduledTime());

		return isValid;
	}
	
	private boolean validateScheduledTime(DateTimeField dateTimeField) {
		if(dateTimeField.getValue().isBefore(LocalDateTime.now())) {
			return Invalidator.invalidateField(dateTimeField, SCHEDULED_DATE_MUST_BE_AFTER_TODAY);
		}
		return true;
	}
	
	private boolean validateName(ScheduledProductDto dto) {
		if (!isFieldPopulated(dto.getName()) || dto.getName().getValue().isEmpty()) {
			return Invalidator.invalidateRequiredField(dto.getName());
		}
		if(productService.productWithNameAndClientIdExits(dto.getName().getValue(), dto.getClient().getId().getValue())) {
			return Invalidator.invalidateField(dto.getName(), ProductValidator.PRODUCT_WITH_THIS_NAME_ALREADY_EXISTS_FOR_THE_CLIENT);
		}
		if(scheduledProductService.productWithNameAndClientIdExits(dto.getName().getValue(), dto.getClient().getId().getValue())) {
			return Invalidator.invalidateField(dto.getName(), SCHEDULED_PRODUCT_WITH_THIS_NAME_ALREADY_EXISTS_FOR_THE_CLIENT);
		}
		return true;
	}

	private static boolean isFieldPopulated(GenericField<?> field) {
		return field.getValue() != null;
	}

}
