package com.example.bitconex.demo.product.model;

import java.util.Arrays;
/**
 * @author dienis.mustafaj
 */
public enum DeviceTypes {
	DSLAM(1), MDU(2), DPU(3);

	private DeviceTypes(int code) {
		this.code = code;
	}

	private int code;

	public int getCode() {
		return code;
	}

	
	public static boolean codeExists(int code) {
		return Arrays.asList(DeviceTypes.values()).stream()
				.anyMatch(technologyType -> (technologyType).getCode() == code);
	}
}
