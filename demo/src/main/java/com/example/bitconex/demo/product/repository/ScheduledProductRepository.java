package com.example.bitconex.demo.product.repository;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import com.example.bitconex.demo.commons.RepositoryTemplate;
import com.example.bitconex.demo.product.model.ScheduledProduct;
import com.example.bitconex.demo.product.model.ScheduledProductStatus;

/**
 * @author dienis.mustafaj
 */
@Repository
public class ScheduledProductRepository extends RepositoryTemplate<ScheduledProduct> {
	
	
	public List<ScheduledProduct> findScheduledProductsReadyToSave() {
		Query query = entityManager.createQuery("select s from ScheduledProduct s where scheduledTime <= :time and status = :status and isDeleted = :isDeleted order by s.scheduledTime ASC");
		query.setParameter("time", LocalDateTime.now());
		query.setParameter("isDeleted", false);
		query.setParameter("status", ScheduledProductStatus.SCHEDULED.getCode());
		return query.getResultList();
	}
	
	public List<ScheduledProduct> findByClientId(Long clientId) {
		Query query = entityManager
				.createQuery("select s from ScheduledProduct s where s.client.id = :clientId and isDeleted = :isDeleted");

		query.setParameter("clientId", clientId);
		query.setParameter("isDeleted", false);

		return query.getResultList();
	}
	
	public boolean productWithNameAndClientIdExits(String name, Long clientId) {
		Query query = entityManager.createQuery(
				"select p from ScheduledProduct p where p.client.id = :clientId and name = :name and isDeleted = :isDeleted");

		query.setParameter("clientId", clientId);
		query.setParameter("name", name);
		query.setParameter("isDeleted", false);

		int size = query.getResultList().size();
		return size > 0;
	}

	public void updateStatusAndMessage(Integer status, String message, Long id) {
		Query query = entityManager.createQuery(
				"update ScheduledProduct s set s.status = :status, s.message = :message where s.id = :id ");
		query.setParameter("status", status);
		query.setParameter("message", message);
		query.setParameter("id", id);
		query.executeUpdate();
	}
}
