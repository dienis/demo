package com.example.bitconex.demo.product.dto;

import com.example.bitconex.demo.client.dto.ClientMapper;
import com.example.bitconex.demo.commons.dtoFields.DateTimeField;
import com.example.bitconex.demo.commons.dtoFields.IntField;
import com.example.bitconex.demo.commons.dtoFields.StringField;
import com.example.bitconex.demo.product.model.ScheduledProduct;
import java.time.LocalDateTime;
import javax.annotation.Generated;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    comments = "version: 1.2.0.Beta2, compiler: javac, environment: Java 1.8.0_151 (Oracle Corporation)"
)
@Component
public class ScheduledProductMapperImpl implements ScheduledProductMapper {

    @Autowired
    private ClientMapper clientMapper;

    @Override
    public ScheduledProductDto toDto(ScheduledProduct product) {
        if ( product == null ) {
            return null;
        }

        ScheduledProductDto scheduledProductDto = new ScheduledProductDto();

        scheduledProductDto.setName( scheduledProductToStringField( product ) );
        scheduledProductDto.setDeviceType( scheduledProductToIntField( product ) );
        scheduledProductDto.setScheduledTime( scheduledProductToDateTimeField( product ) );
        scheduledProductDto.setUpstream( scheduledProductToStringField( product ) );
        scheduledProductDto.setTechnologyType( scheduledProductToIntField( product ) );
        scheduledProductDto.setId( product.getId() );
        scheduledProductDto.setClient( clientMapper.toDto( product.getClient() ) );
        scheduledProductDto.setStatus( product.getStatus() );
        scheduledProductDto.setMessage( product.getMessage() );

        return scheduledProductDto;
    }

    @Override
    public ScheduledProduct toEntity(ScheduledProductDto productDto) {
        if ( productDto == null ) {
            return null;
        }

        ScheduledProduct scheduledProduct = new ScheduledProduct();

        Integer value = productDtoDeviceTypeValue( productDto );
        if ( value != null ) {
            scheduledProduct.setDeviceType( value );
        }
        LocalDateTime value1 = productDtoScheduledTimeValue( productDto );
        if ( value1 != null ) {
            scheduledProduct.setScheduledTime( value1 );
        }
        String value2 = productDtoUpstreamValue( productDto );
        if ( value2 != null ) {
            scheduledProduct.setUpstream( value2 );
        }
        String value3 = productDtoNameValue( productDto );
        if ( value3 != null ) {
            scheduledProduct.setName( value3 );
        }
        Integer value4 = productDtoTechnologyTypeValue( productDto );
        if ( value4 != null ) {
            scheduledProduct.setTechnologyType( value4 );
        }
        scheduledProduct.setId( productDto.getId() );
        scheduledProduct.setClient( clientMapper.toEntity( productDto.getClient() ) );
        scheduledProduct.setStatus( productDto.getStatus() );
        scheduledProduct.setMessage( productDto.getMessage() );

        return scheduledProduct;
    }

    protected StringField scheduledProductToStringField(ScheduledProduct scheduledProduct) {
        if ( scheduledProduct == null ) {
            return null;
        }

        StringField stringField = new StringField();

        stringField.setValue( scheduledProduct.getName() );

        return stringField;
    }

    protected IntField scheduledProductToIntField(ScheduledProduct scheduledProduct) {
        if ( scheduledProduct == null ) {
            return null;
        }

        IntField intField = new IntField();

        intField.setValue( scheduledProduct.getDeviceType() );

        return intField;
    }

    protected DateTimeField scheduledProductToDateTimeField(ScheduledProduct scheduledProduct) {
        if ( scheduledProduct == null ) {
            return null;
        }

        DateTimeField dateTimeField = new DateTimeField();

        dateTimeField.setValue( scheduledProduct.getScheduledTime() );

        return dateTimeField;
    }

    private Integer productDtoDeviceTypeValue(ScheduledProductDto scheduledProductDto) {
        if ( scheduledProductDto == null ) {
            return null;
        }
        IntField deviceType = scheduledProductDto.getDeviceType();
        if ( deviceType == null ) {
            return null;
        }
        Integer value = deviceType.getValue();
        if ( value == null ) {
            return null;
        }
        return value;
    }

    private LocalDateTime productDtoScheduledTimeValue(ScheduledProductDto scheduledProductDto) {
        if ( scheduledProductDto == null ) {
            return null;
        }
        DateTimeField scheduledTime = scheduledProductDto.getScheduledTime();
        if ( scheduledTime == null ) {
            return null;
        }
        LocalDateTime value = scheduledTime.getValue();
        if ( value == null ) {
            return null;
        }
        return value;
    }

    private String productDtoUpstreamValue(ScheduledProductDto scheduledProductDto) {
        if ( scheduledProductDto == null ) {
            return null;
        }
        StringField upstream = scheduledProductDto.getUpstream();
        if ( upstream == null ) {
            return null;
        }
        String value = upstream.getValue();
        if ( value == null ) {
            return null;
        }
        return value;
    }

    private String productDtoNameValue(ScheduledProductDto scheduledProductDto) {
        if ( scheduledProductDto == null ) {
            return null;
        }
        StringField name = scheduledProductDto.getName();
        if ( name == null ) {
            return null;
        }
        String value = name.getValue();
        if ( value == null ) {
            return null;
        }
        return value;
    }

    private Integer productDtoTechnologyTypeValue(ScheduledProductDto scheduledProductDto) {
        if ( scheduledProductDto == null ) {
            return null;
        }
        IntField technologyType = scheduledProductDto.getTechnologyType();
        if ( technologyType == null ) {
            return null;
        }
        Integer value = technologyType.getValue();
        if ( value == null ) {
            return null;
        }
        return value;
    }
}
