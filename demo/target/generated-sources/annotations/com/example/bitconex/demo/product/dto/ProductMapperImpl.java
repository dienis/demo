package com.example.bitconex.demo.product.dto;

import com.example.bitconex.demo.client.dto.ClientMapper;
import com.example.bitconex.demo.commons.dtoFields.BooleanField;
import com.example.bitconex.demo.commons.dtoFields.IntField;
import com.example.bitconex.demo.commons.dtoFields.StringField;
import com.example.bitconex.demo.product.model.Product;
import javax.annotation.Generated;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    comments = "version: 1.2.0.Beta2, compiler: javac, environment: Java 1.8.0_151 (Oracle Corporation)"
)
@Component
public class ProductMapperImpl implements ProductMapper {

    @Autowired
    private ClientMapper clientMapper;

    @Override
    public ProductDto toDto(Product product) {
        if ( product == null ) {
            return null;
        }

        ProductDto productDto = new ProductDto();

        productDto.setName( productToStringField( product ) );
        productDto.setDeviceType( productToIntField( product ) );
        productDto.setIsActive( productToBooleanField( product ) );
        productDto.setUpstream( productToStringField( product ) );
        productDto.setTechnologyType( productToIntField( product ) );
        productDto.setId( product.getId() );
        productDto.setClient( clientMapper.toDto( product.getClient() ) );

        return productDto;
    }

    @Override
    public Product toEntity(ProductDto productDto) {
        if ( productDto == null ) {
            return null;
        }

        Product product = new Product();

        Integer value = productDtoDeviceTypeValue( productDto );
        if ( value != null ) {
            product.setDeviceType( value );
        }
        String value1 = productDtoUpstreamValue( productDto );
        if ( value1 != null ) {
            product.setUpstream( value1 );
        }
        String value2 = productDtoNameValue( productDto );
        if ( value2 != null ) {
            product.setName( value2 );
        }
        Integer value3 = productDtoTechnologyTypeValue( productDto );
        if ( value3 != null ) {
            product.setTechnologyType( value3 );
        }
        product.setId( productDto.getId() );
        Boolean value4 = productDtoIsActiveValue( productDto );
        if ( value4 != null ) {
            product.setIsActive( value4 );
        }
        product.setClient( clientMapper.toEntity( productDto.getClient() ) );

        return product;
    }

    protected StringField productToStringField(Product product) {
        if ( product == null ) {
            return null;
        }

        StringField stringField = new StringField();

        stringField.setValue( product.getName() );

        return stringField;
    }

    protected IntField productToIntField(Product product) {
        if ( product == null ) {
            return null;
        }

        IntField intField = new IntField();

        intField.setValue( product.getDeviceType() );

        return intField;
    }

    protected BooleanField productToBooleanField(Product product) {
        if ( product == null ) {
            return null;
        }

        BooleanField booleanField = new BooleanField();

        booleanField.setValue( product.getIsActive() );

        return booleanField;
    }

    private Integer productDtoDeviceTypeValue(ProductDto productDto) {
        if ( productDto == null ) {
            return null;
        }
        IntField deviceType = productDto.getDeviceType();
        if ( deviceType == null ) {
            return null;
        }
        Integer value = deviceType.getValue();
        if ( value == null ) {
            return null;
        }
        return value;
    }

    private String productDtoUpstreamValue(ProductDto productDto) {
        if ( productDto == null ) {
            return null;
        }
        StringField upstream = productDto.getUpstream();
        if ( upstream == null ) {
            return null;
        }
        String value = upstream.getValue();
        if ( value == null ) {
            return null;
        }
        return value;
    }

    private String productDtoNameValue(ProductDto productDto) {
        if ( productDto == null ) {
            return null;
        }
        StringField name = productDto.getName();
        if ( name == null ) {
            return null;
        }
        String value = name.getValue();
        if ( value == null ) {
            return null;
        }
        return value;
    }

    private Integer productDtoTechnologyTypeValue(ProductDto productDto) {
        if ( productDto == null ) {
            return null;
        }
        IntField technologyType = productDto.getTechnologyType();
        if ( technologyType == null ) {
            return null;
        }
        Integer value = technologyType.getValue();
        if ( value == null ) {
            return null;
        }
        return value;
    }

    private Boolean productDtoIsActiveValue(ProductDto productDto) {
        if ( productDto == null ) {
            return null;
        }
        BooleanField isActive = productDto.getIsActive();
        if ( isActive == null ) {
            return null;
        }
        Boolean value = isActive.getValue();
        if ( value == null ) {
            return null;
        }
        return value;
    }
}
