package com.example.bitconex.demo.client.dto;

import com.example.bitconex.demo.client.model.Client;
import com.example.bitconex.demo.commons.dtoFields.LongField;
import com.example.bitconex.demo.commons.dtoFields.StringField;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    comments = "version: 1.2.0.Beta2, compiler: javac, environment: Java 1.8.0_151 (Oracle Corporation)"
)
@Component
public class ClientMapperImpl implements ClientMapper {

    @Override
    public ClientDto toDto(Client client) {
        if ( client == null ) {
            return null;
        }

        ClientDto clientDto = new ClientDto();

        clientDto.setId( clientToLongField( client ) );
        clientDto.setEmail( clientToStringField( client ) );
        clientDto.setName( clientToStringField( client ) );
        clientDto.setSurname( clientToStringField( client ) );

        return clientDto;
    }

    @Override
    public Client toEntity(ClientDto clientDto) {
        if ( clientDto == null ) {
            return null;
        }

        Client client = new Client();

        String value = clientDtoSurnameValue( clientDto );
        if ( value != null ) {
            client.setSurname( value );
        }
        String value1 = clientDtoNameValue( clientDto );
        if ( value1 != null ) {
            client.setName( value1 );
        }
        Long value2 = clientDtoIdValue( clientDto );
        if ( value2 != null ) {
            client.setId( value2 );
        }
        String value3 = clientDtoEmailValue( clientDto );
        if ( value3 != null ) {
            client.setEmail( value3 );
        }

        return client;
    }

    protected LongField clientToLongField(Client client) {
        if ( client == null ) {
            return null;
        }

        LongField longField = new LongField();

        longField.setValue( client.getId() );

        return longField;
    }

    protected StringField clientToStringField(Client client) {
        if ( client == null ) {
            return null;
        }

        StringField stringField = new StringField();

        stringField.setValue( client.getEmail() );

        return stringField;
    }

    private String clientDtoSurnameValue(ClientDto clientDto) {
        if ( clientDto == null ) {
            return null;
        }
        StringField surname = clientDto.getSurname();
        if ( surname == null ) {
            return null;
        }
        String value = surname.getValue();
        if ( value == null ) {
            return null;
        }
        return value;
    }

    private String clientDtoNameValue(ClientDto clientDto) {
        if ( clientDto == null ) {
            return null;
        }
        StringField name = clientDto.getName();
        if ( name == null ) {
            return null;
        }
        String value = name.getValue();
        if ( value == null ) {
            return null;
        }
        return value;
    }

    private Long clientDtoIdValue(ClientDto clientDto) {
        if ( clientDto == null ) {
            return null;
        }
        LongField id = clientDto.getId();
        if ( id == null ) {
            return null;
        }
        Long value = id.getValue();
        if ( value == null ) {
            return null;
        }
        return value;
    }

    private String clientDtoEmailValue(ClientDto clientDto) {
        if ( clientDto == null ) {
            return null;
        }
        StringField email = clientDto.getEmail();
        if ( email == null ) {
            return null;
        }
        String value = email.getValue();
        if ( value == null ) {
            return null;
        }
        return value;
    }
}
